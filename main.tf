resource "azurerm_public_ip" "gateway" {
  name                = "gateway"
  resource_group_name = var.vnet_resource_group_name
  location            = var.location
  sku                 = "Standard"
  allocation_method   = "Static"
  zones               = [1, 2, 3]
}

locals {
  kv-shared-key = "vpn-shared-key"
}

resource "random_string" "kv_suffix" {
  length  = 3
  special = false
  upper   = false
  keepers = {
    name = var.name
  }
}

module "key_vault" {
  source                = "gitlab.com/operator-ict/key-vault/azurerm"
  version               = "0.0.1"
  name                  = "${var.name}-${random_string.kv_suffix.result}"
  resource_group_name   = var.vnet_resource_group_name
  location              = var.location
  expected_kv_passwords = [local.kv-shared-key]
}

resource "azurerm_virtual_network_gateway" "gateway" {
  name                = "gateway"
  resource_group_name = var.vnet_resource_group_name
  location            = azurerm_public_ip.gateway.location

  type          = "Vpn"
  vpn_type      = "RouteBased"
  active_active = false
  enable_bgp    = false
  sku           = var.sku
  generation    = "Generation1"

  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = azurerm_public_ip.gateway.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = var.subnet_id
  }

  dynamic "vpn_client_configuration" {
    for_each = length(var.tenant_id) < 1 ? [] : ["X"]
    content {
      address_space        = ["172.31.0.0/16"]
      vpn_client_protocols = ["OpenVPN"]
      vpn_auth_types       = ["AAD"]
      aad_tenant           = "https://login.microsoftonline.com/${var.tenant_id}"
      aad_audience         = "41b23e61-6c1e-4545-b367-cd054e0ed4b4"
      aad_issuer           = "https://sts.windows.net/${var.tenant_id}/"
    }
  }

}

locals {
  # extract CIDRs of remote network
  address_space = {for net_key, net_val in var.local_networks : net_key => sort(distinct(flatten([
      for pair in net_val.traffic_selector_policies : [pair.remote_address_cidrs]
    ])))
  }
}

resource "azurerm_local_network_gateway" "lng" {
  for_each            = var.local_networks
  name                = "lng-${each.key}"
  location            = azurerm_public_ip.gateway.location
  resource_group_name = var.vnet_resource_group_name
  gateway_address     = each.value.gateway_address
  address_space       = local.address_space[each.key]
}

resource "azurerm_virtual_network_gateway_connection" "vngc" {
  for_each            = var.local_networks
  name                = "vngc-${azurerm_virtual_network_gateway.gateway.name}-${each.key}"
  location            = azurerm_public_ip.gateway.location
  resource_group_name = var.vnet_resource_group_name

  connection_protocol        = "IKEv2"
  type                       = "IPsec"
  virtual_network_gateway_id = azurerm_virtual_network_gateway.gateway.id
  local_network_gateway_id   = azurerm_local_network_gateway.lng[each.key].id

  ipsec_policy {
    dh_group         = "ECP256"
    ike_encryption   = "AES256"
    ike_integrity    = "SHA256"
    ipsec_encryption = "AES128"
    ipsec_integrity  = "SHA256"
    pfs_group        = "ECP256"
    sa_lifetime      = 86400
  }
  shared_key = module.key_vault.expected_secrets[local.kv-shared-key].value

  dynamic "traffic_selector_policy" {
    for_each = each.value.traffic_selector_policies
    content {
      local_address_cidrs  = traffic_selector_policy.value.local_address_cidrs
      remote_address_cidrs = traffic_selector_policy.value.remote_address_cidrs
    }
  }
}
