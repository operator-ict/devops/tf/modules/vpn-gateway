variable "location" {
  type = string
}

variable "name" {
  type = string
}

variable "vnet_id" {
  type = string
}

variable "vnet_name" {
  type = string
}

variable "vnet_resource_group_name" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "tags" {
  type = map(string)
}

variable "sku" {
  type = string
}

variable "address_prefixes" {
  type = list(string)
}

variable "local_networks" {
  type = map(object({
    gateway_address      = string
    traffic_selector_policies = map(object({
      local_address_cidrs  = list(string)
      remote_address_cidrs = list(string)
    }))
  }))
  default = {}
}

variable "tenant_id" {
  type = string
}
